import requests
import json
from requests import Response
from os import path

from constants.cards import ygo_pro_url, cards_path


def pull_latest_cards():
    print("Grabbing latest cards")
    cards_url: str = f'{ygo_pro_url}/cardinfo.php'
    res: Response = requests.get(cards_url)
    cards = res.json()['data']
    with open(path.abspath(cards_path), 'w') as file:
        cards_str: str = json.dumps(cards)
        file.write(cards_str)

if __name__ == '__main__':
    pull_latest_cards()
