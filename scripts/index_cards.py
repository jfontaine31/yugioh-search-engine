import json

from typing import List, Dict
from os import path
from multiprocessing import cpu_count

from concurrent.futures import ThreadPoolExecutor, Future, as_completed
from constants.cards import cards_path
from controllers.elasticsearch_controller import ElasticSearchController


def index_cards() -> None:
    cards: List[Dict] = None
    print('indexing cards in elastic search')

    with open(path.abspath(cards_path), 'r') as file:
        cards = json.load(file)

    cpu_cores: int = cpu_count() // 2
    print(f'number of cpus: {cpu_cores}')
    futures: List[Future] = []
    with ThreadPoolExecutor(max_workers=cpu_cores) as executor:
        future = executor.submit(ElasticSearchController.index_documents, cards)
        futures.append(future)
    as_completed(futures)

if __name__ == '__main__':
    index_cards()
