from typing import List, Dict
from elasticsearch import Elasticsearch
from controllers.ygo_card_formatter import YgoCardFormatter
from constants.cards import ygo_es_index

es = Elasticsearch(['http://localhost:9200'])


class ElasticSearchController:
    @staticmethod
    def index_documents(cards: List[Dict]) -> None:
        for card in cards:
            print(f"Indexing card: {card['id']} {card['name']}")
            formatted_card: Dict = YgoCardFormatter.format_for_es(card)
            id: int = formatted_card['id']
            es.index(index=ygo_es_index, id=id, body=formatted_card)

    @staticmethod
    def search_documents(query: str) -> Dict:
        es_body = {
            "query": {
                "multi_match": {
                    "query": query,
                    "fields": ['name', "desc"]
                }
            }
        }
        docs: Dict = es.search(index=ygo_es_index, body=es_body)

        return docs
