from typing import Dict, List


'''
{
  "id": 34541863,
  "name": "\"A\" Cell Breeding Device",
  "type": "Spell Card",
  "desc": "During each of your Standby Phases, put 1 A-Counter on 1 face-up monster your opponent controls.",
  "race": "Continuous",
  "archetype": "Alien",
  "card_sets": [
    {
      "set_name": "Force of the Breaker",
      "set_code": "FOTB-EN043",
      "set_rarity": "Common",
      "set_rarity_code": "(C)",
      "set_price": "1.03"
    }
  ],
  "card_images": [
    {
      "id": 34541863,
      "image_url": "https://storage.googleapis.com/ygoprodeck.com/pics/34541863.jpg",
      "image_url_small": "https://storage.googleapis.com/ygoprodeck.com/pics_small/34541863.jpg"
    }
  ],
  "card_prices": [
    {
      "cardmarket_price": "0.09",
      "tcgplayer_price": "0.17",
      "ebay_price": "8.00",
      "amazon_price": "0.25",
      "coolstuffinc_price": "0.25"
    }
  ]
}
'''

class YgoCardFormatter:
    @staticmethod
    def format_for_es(card: Dict) -> Dict:

        return card

