from flask import Flask

from routes.ygo_search import ygo_search

app = Flask(__name__)

# registering blueprints
app.register_blueprint(ygo_search, url_prefix="/ygo_search")


@app.route('/')
def hello_world():
    return "Hello World"


if __name__ == '__main__':
    app.run(host="0.0.0.0")