from flask import Blueprint, request

from controllers.elasticsearch_controller import ElasticSearchController

ygo_search = Blueprint('ygo_search', __name__)


@ygo_search.route('/', methods=['GET'])
def search():
    query: str = request.args.get('query', '')

    return ElasticSearchController.search_documents(query)
